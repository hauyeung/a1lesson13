package com.example.lesson13;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity {

	Button notebutton;
	Button stockbutton;
	TextView hellotextview;
	String welcomename;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		hellotextview = (TextView) findViewById(R.id.welcome);
		notebutton = (Button) findViewById(R.id.notebutton);
		stockbutton = (Button) findViewById(R.id.stockbutton);
		String name = getPreferences(MODE_PRIVATE).getString("name", "");
		if (name == "")
		{
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			final EditText nameinput = new EditText(this);
			builder.setTitle("Please enter your name");
			builder.setView(nameinput);
			builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					final SharedPreferences prefs = (SharedPreferences) getPreferences(MODE_PRIVATE);
					final SharedPreferences.Editor editor = prefs.edit();
					String newname = nameinput.getText().toString();
					welcomename = newname;
					editor.putString("name",newname);
					editor.commit();					
					hellotextview.setText("Hello, "+newname);
					}
				}
			);
			builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener(){

				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					
				}
				
			});
			AlertDialog dialog = builder.create();
			dialog.show();
		}
		else
		{
			welcomename = name;
			hellotextview.setText("Hello, "+name);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	public void startnote(View view)
	{
		Intent intent = new Intent(MainActivity.this, ViewNote.class);
		intent.putExtra("name",welcomename);
		startActivity(intent);
	}
	
	public void startstock(View view)
	{
		Intent intent = new Intent(MainActivity.this, Weather.class);
		intent.putExtra("name",welcomename);
		startActivity(intent);
	}

}
