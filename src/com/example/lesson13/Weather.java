package com.example.lesson13;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.json.JSONArray;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.support.v4.app.NavUtils;
import android.annotation.TargetApi;
import android.os.Build;

public class Weather extends Activity {
	Bundle extras;
	TextView welcometextview, citytextview,forcecastdatetextview, forecasthightextview, forecastlowtextview;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_weather);
		
		welcometextview = (TextView) findViewById(R.id.welcome);
		citytextview = (TextView) findViewById(R.id.citytextview);
		forcecastdatetextview = (TextView) findViewById(R.id.forecastdatetextview);
		forecasthightextview = (TextView) findViewById(R.id.forecasthightextview);
		forecastlowtextview = (TextView) findViewById(R.id.forecastlowtextview);
		// Show the Up button in the action bar.
		setupActionBar();
		extras = getIntent().getExtras();
		if (extras != null)
		{
			String name = extras.getString("name").toString();
			if (name != "")
			{
				welcometextview.setText("Hello, "+extras.getString("name"));
			}
		}		
		
	}
	
	public void getstock(View view)
	{
		new getstockinfo().execute();
	}

	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.stock_ticker, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	private class getstockinfo extends AsyncTask<String,String,String>
	{
		String city = "", country = "", region = "", forecastdate = "", forecasthigh = "", forecastlow = "";
		@Override
		protected String doInBackground(String... arg0) {
			DefaultHttpClient httpclient = new DefaultHttpClient(new BasicHttpParams());			
			String url = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%3D2502265&format=json&diagnostics=true&callback=";
			HttpPost httppost = new HttpPost(url);
			httppost.setHeader("content0type","application-json");
			InputStream inputstream = null;
			String result = null;
			try
			{
				HttpResponse response = httpclient.execute(httppost);
				HttpEntity entity = response.getEntity();
				inputstream = entity.getContent();
				BufferedReader reader = new BufferedReader(new InputStreamReader(inputstream, "UTF-8"),8);
				StringBuilder stringbuilder = new StringBuilder();
				String line = null;
				while ((line = reader.readLine()) != null)
				{
					stringbuilder.append(line+"\n");
				}
				
				result = stringbuilder.toString();
				Log.v("result",result);
			}
			catch (Exception e)
			{
				
			}
			finally
			{
				try
				{
					if (inputstream != null)
						inputstream.close();
				}
				catch (Exception e)
				{
					
				}
			}
			
			JSONObject jsonobject;
			try
			{
				jsonobject = new JSONObject(result);
				JSONObject queryobj = jsonobject.getJSONObject("query");
				JSONObject resultobj = queryobj.getJSONObject("results");
				JSONObject channelobj = resultobj.getJSONObject("channel");
				JSONObject locationobj = channelobj.getJSONObject("location");
				JSONObject itemobj = channelobj.getJSONObject("item");
				JSONArray forecastobj = itemobj.getJSONArray("forecast");				
				city = locationobj.getString("city");
				country = locationobj.getString("country");
				region = locationobj.getString("region");
				JSONObject for1obj = forecastobj.getJSONObject(0);
				forecastdate = for1obj.getString("date");
				forecasthigh = for1obj.getString("high");
				forecastlow = for1obj.getString("low");
				
				Log.v("city", locationobj.toString());
				Log.v("country", country);
				Log.v("region", region);
			
				
				
			}
			catch (Exception e)
			{
				Log.v("error",e.getMessage());
			}
			return result;
		}
		
		protected void onPostExecute(String result)
		{
			citytextview.setText(city+", "+region+", "+country);
			forcecastdatetextview.setText(forecastdate);
			forecasthightextview.setText("High: "+forecasthigh+" F");
			forecastlowtextview.setText("Low: "+forecastlow+" F");
		}
		
	}
	

}
;