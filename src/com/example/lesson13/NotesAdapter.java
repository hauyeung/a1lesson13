package com.example.lesson13;

import java.util.ArrayList;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class NotesAdapter extends ArrayAdapter<Note> {
	
	public ArrayList<Note> notes;
	public Context context;
	public NotesAdapter(Context context, ArrayList<Note> notes)
	{
		super(context, R.layout.notelayout, notes);
		this.context = context;
		this.notes = notes;		
		
	}
	
	  @Override
	  public View getView(int position, View convertView, ViewGroup parent) {
	    LayoutInflater inflater = (LayoutInflater) context
	        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	    View rowView = inflater.inflate(R.layout.notelayout, parent, false);
	    TextView idtextview = (TextView) rowView.findViewById(R.id.id);
	    TextView contenttextview = (TextView) rowView.findViewById(R.id.content);
	    idtextview.setText(((Integer)notes.get(position).id).toString());
	    contenttextview.setText(String.format("%s - %s",notes.get(position).title ,notes.get(position).content));
	    return rowView;
	  }



}
