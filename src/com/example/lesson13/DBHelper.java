package com.example.lesson13;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {

    private static final String DB_NAME = "notes.db";
    private static final int DB_VERSION = 3;
    public static final String TABLE_NOTE = "notes";
    public static final String C_ID = "_id";
    public static final String C_TITLE = "title";
    public static final String C_CONTENT = "content";

    public DBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);        
    }
    
  @Override
  public void onCreate(SQLiteDatabase db) {
      final String sqlCreateTablePeople = "create table notes (id integer primary key autoincrement, title text not null, content text not null);";
          db.execSQL(sqlCreateTablePeople);
          
      final String seed = "insert into notes (id, title, content) values (1,'title', 'content');";
      db.execSQL(seed);
  }

  @Override
  public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    // TODO Auto-generated method stub
	  final String sqlDropTablePeople = "DROP TABLE IF EXISTS notes;";
      db.execSQL(sqlDropTablePeople);
      onCreate(db);
  }
  
  
}

