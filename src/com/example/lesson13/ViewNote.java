package com.example.lesson13;

import java.util.ArrayList;

import android.os.AsyncTask;
import android.os.Bundle;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.support.v4.app.NavUtils;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.database.Cursor;
import android.database.CursorJoiner.Result;
import android.database.sqlite.SQLiteDatabase;

public class ViewNote extends ListActivity {

	private SQLiteDatabase db;
	private DBHelper dbhelper;
	private ArrayList<Note> notes;
	private Note note;
	private ListView noteslistview;
	private NotesAdapter adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);		
		setContentView(R.layout.activity_view_note);
		// Show the Up button in the action bar.
		dbhelper = new DBHelper(this);
		this.notes = new ArrayList<Note>();
		db = dbhelper.getWritableDatabase();
		updatelistview();
	}
	
	private void updatelistview()
	{
		if (!notes.isEmpty())
			notes.clear();
		
		Cursor cursor = db.rawQuery("select * from notes",null);
		cursor.moveToFirst();
		while(cursor.isAfterLast() == false)
		{
			note = new Note();
			note.id = cursor.getInt(cursor
                    .getColumnIndex("id"));
			note.title = cursor.getString(cursor
                    .getColumnIndex("title"));
			note.content = cursor.getString(cursor
                    .getColumnIndex("content"));
            notes.add(note);
            cursor.moveToNext();
		}
		cursor.close();
		adapter = new NotesAdapter(this,notes);
		noteslistview = (ListView) findViewById(R.id.noteslistview);
		noteslistview.setAdapter(adapter);
		registerForContextMenu(noteslistview);
	}
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo)
	{
		super.onCreateContextMenu(menu, v, menuInfo);
		getMenuInflater().inflate(R.menu.notecontextmenu,menu);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.view_note, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public boolean onContextItemSelected(MenuItem item)
	{	
		AdapterContextMenuInfo menuinfo = (AdapterContextMenuInfo) item.getMenuInfo();
		Note note = (Note) noteslistview.getItemAtPosition(menuinfo.position);
		int id = note.id;
		switch (item.getItemId()) {
		case R.id.edit:
			edit(id);
			return true;
		case R.id.delete:
			delete(id);
			return true;
		}
		return super.onContextItemSelected(item);
	}
	
	public void edit(final int id)
	{
		final EditText nameinput = new EditText(this);
		nameinput.setHint("Name");
		final EditText noteinput = new EditText(this);
		noteinput.setHint("Note Content");
		LinearLayout editlinear = new LinearLayout(this);
		editlinear.setOrientation(LinearLayout.VERTICAL);
		final AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Please enter");		
		editlinear.addView(nameinput);
		editlinear.addView(noteinput);
		builder.setView(editlinear);
		builder.setPositiveButton("OK", new OnClickListener(){

			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				try
				{
					new edit(id, nameinput.getText().toString(), noteinput.getText().toString()).execute();
					updatelistview();
				}
				catch (Exception e)
				{
					
				}

			}
			
		});
		builder.setNegativeButton("Cancel", new OnClickListener(){

			@Override
			public void onClick(DialogInterface dialog, int which) {
				finish();
				
			}
			
		});
		AlertDialog editalert = builder.create();
		editalert.show();
	}
	
	public void delete(final int id)
	{
		AlertDialog.Builder deletebuilder = new AlertDialog.Builder(this);
		deletebuilder.setTitle("Delete");
		deletebuilder.setMessage("Delete note?");
		deletebuilder.setPositiveButton("OK", new OnClickListener(){

			@Override
			public void onClick(DialogInterface dialog, int which) {
				try
				{
					new delete(id).execute();
					updatelistview();
				}
				catch(Exception e)
				{
					
				}

			}
			
		});
		deletebuilder.setNegativeButton("Cancel", new OnClickListener(){

			@Override
			public void onClick(DialogInterface dialog, int which) {

				
			}
			
		});
		AlertDialog deletealert = deletebuilder.create();
		deletealert.show();
		
	}
	
	private class add extends AsyncTask<Void,Void,Void>
	{
		String title, content;
		public add(String title, String content)
		{
			this.title = title;
			this.content = content;
		}
		@Override
		protected Void doInBackground(Void... params) {
		
			try
			{
				db = dbhelper.getWritableDatabase();
				ContentValues cv = new ContentValues();
				cv.put("title", title);
				cv.put("content", content);			
				db.insert("notes", null, cv);								
				
			}
			catch (Exception e)
			{
				
			}
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result)
		{
			updatelistview();
			db.close();
		}
		
	}
	
	private class edit extends AsyncTask<Void,Void,Void>
	{
		int id;
		String title, content;
		public edit(int id2, String title, String content)
		{
			this.id = id2;
			this.title = title;
			this.content = content;
		}
		@Override
		protected Void doInBackground(Void... params) {
			try
			{
				db = dbhelper.getWritableDatabase();
				ContentValues cv = new ContentValues();
				cv.put("title", title);
				cv.put("content", content);			
				db.update("notes", cv, "id="+id, null);
				

			}
			catch (Exception e)
			{
				
			}
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result)
		{
			updatelistview();
			db.close();
		}
		
	}
	
	private class delete extends AsyncTask<Void,Void,Void>
	{
		int id;
		public delete(int id2)
		{
			this.id = id2;
		}
		@Override
		protected Void doInBackground(Void... params) {
			try
			{
			  db = dbhelper.getWritableDatabase();
			  db.delete("notes","id="+id, null);			 

			}
			catch (Exception e)
			{
				
			}
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result)
		{
			updatelistview();
			db.close();
		}
		
	}
	
	public void addnote(View view)
	{
		final EditText nameinput = new EditText(this);
		nameinput.setHint("Name");
		final EditText noteinput = new EditText(this);
		noteinput.setHint("Note Content");
		LinearLayout editlinear = new LinearLayout(this);
		editlinear.setOrientation(LinearLayout.VERTICAL);
		final AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Please enter");		
		editlinear.addView(nameinput);
		editlinear.addView(noteinput);
		builder.setView(editlinear);
		builder.setPositiveButton("OK", new OnClickListener(){

			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				try
				{
					new add(nameinput.getText().toString(), noteinput.getText().toString()).execute();
					updatelistview();					
				}
				catch (Exception e)
				{
					
				}
			}
			
		});
		builder.setNegativeButton("Cancel", new OnClickListener(){

			@Override
			public void onClick(DialogInterface dialog, int which) {
				finish();
				
			}
			
		});
		AlertDialog addnotealert = builder.create();
		addnotealert.show();
	}

}
