package com.example.lesson13;

import java.util.ArrayList;

import android.database.sqlite.SQLiteDatabase;

public class NoteRepo {
	
	private SQLiteDatabase db;
	private DBHelper dbhelper;
	  public void editnote(int id, String content)
	  {
		  db = dbhelper.getWritableDatabase();
		  final String updatenote = String.format("update notes set content=%s where id=%i;",content,id);
		  db.execSQL(updatenote);	  
	  }
	  
	  public void deletenote(int id)
	  {
		  db = dbhelper.getWritableDatabase();
		  final String deletenote = String.format("delete from notes where id=%i", id);
		  db.execSQL(deletenote);
	  }
	  
	  public void addnote(String title, String content)
	  {
		  db = dbhelper.getWritableDatabase();
		  final String addnote = String.format("insert into notes (%s, %s);",title, content);
		  db.execSQL(addnote);
	  }
	 
}
