package com.example.lesson13;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.support.v4.app.NavUtils;
import android.annotation.TargetApi;
import android.os.Build;

public class StockTicker extends Activity {
	Bundle extras;
	TextView welcometextview, stockinfotextview;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_stock_ticker);
		welcometextview = (TextView) findViewById(R.id.welcome);
		stockinfotextview = (TextView) findViewById(R.id.stockinfo);
		// Show the Up button in the action bar.
		setupActionBar();
		extras = getIntent().getExtras();
		if (extras != null)
		{
			String name = extras.getString("name").toString();
			if (name != "")
			{
				welcometextview.setText("Hello, "+extras.getString("name"));
			}
		}		
		new getstockinfo().execute();
	}

	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.stock_ticker, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	private class getstockinfo extends AsyncTask<Void,Void,Void>
	{

		@Override
		protected Void doInBackground(Void... arg0) {
			HttpClient httpclient = new DefaultHttpClient();
			String url = "http://finance.google.com/finance/info?client=ig&q=NASDAQ:GOOG";
			HttpGet httpget = new HttpGet(url);
			HttpResponse response;
			InputStream inputstream;
			try
			{
				response = httpclient.execute(httpget);
				HttpEntity entity = response.getEntity();
				String result = null;
				if (entity != null)
				{
					inputstream = entity.getContent();
					BufferedReader reader = new BufferedReader(new InputStreamReader(inputstream, "UTF-8"),8);
					StringBuilder sb = new StringBuilder();
					String line = null;
					while((line = reader.readLine()) != null)
					{
						sb.append(line+"\n");
					}
					result = sb.toString();
				}
				stockinfotextview.setText(result);
				
			}
			catch (Exception e)
			{
				
			}
			return null;
		}
		
	}
	

}
